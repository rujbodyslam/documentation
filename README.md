# PeerTube documentation

## Install

Run npm install, and copy files in `vendor/`.

```
$ ./install.sh
```

## Run

```
$ npm run docsify serve
```
